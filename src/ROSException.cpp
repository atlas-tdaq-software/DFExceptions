/*
  ATLAS ROS Software

  Class: ROSEXCEPTION
  Authors: ATLAS ROS group 	
*/

#include "DFExceptions/ROSException.h"

ROSException::ROSException(const ers::Context & context)
   : ers::Issue( context ), m_package(0), m_errorId(0), m_line(0) {
}


ROSException::ROSException(std::string package,unsigned int errorId,std::string errorText,
                           const ers::Context& context)
   : ers::Issue(context, errorText.c_str()),
     m_package(package),
     m_errorId(errorId),
     m_errorText(errorText),
     m_printSpecificDescription(false),
     m_printFilename(false),
     m_line(0) {
}

ROSException::ROSException(std::string package,unsigned int errorId,std::string errorText,
                           std::string description, const ers::Context& context)
   : ers::Issue(context, description),
     m_package(package),
     m_errorId(errorId),
     m_errorText(errorText),
     m_specificDescription(description),
     m_printSpecificDescription(true),
     m_printFilename(false),
     m_line(0) { 
   prepend_message(": ");
   prepend_message(errorText.c_str());
}

ROSException::ROSException(std::string package,unsigned int errorId,std::string errorText) 
   : ers::Issue(ers::LocalContext(package.c_str(), "unknown", 0, "unknown"), errorText.c_str()),
     m_package(package),
     m_errorId(errorId),
     m_errorText(errorText),
     m_printSpecificDescription(false),
     m_printFilename(false),
     m_line(0) {
}

ROSException::ROSException(std::string package,unsigned int errorId,std::string errorText,
			   std::string description) 
   : ers::Issue(ers::LocalContext(package.c_str(), "unknown", 0, "unknown"), description),
     m_package(package),
     m_errorId(errorId),
     m_errorText(errorText),
     m_specificDescription(description),
     m_printSpecificDescription(true),
     m_printFilename(false),
     m_line(0) {
   prepend_message(": ");
   prepend_message(errorText.c_str());
}


ROSException::ROSException(const std::exception& cause, std::string package, unsigned int errorId, std::string errorText,
                           std::string description, const ers::Context& context)
   : ers::Issue(context,cause), 
     m_package(package),
     m_errorId(errorId),
     m_errorText(errorText),
     m_specificDescription(description),
     m_printSpecificDescription(true),
     m_printFilename(false),
     m_line(0) { 

   prepend_message(description);
   prepend_message(": ");
   prepend_message(errorText);
}

const std::string ROSException::getPackage() const {
  return m_package;
}

unsigned int ROSException::getErrorId() const {
  return m_errorId;
}

std::ostream & ROSException::printErrorMessage(std::ostream &str) const { 
  str << "Package " << m_package << ", "
      << "error " << m_errorId << ":  " ;
  printDescription(str);
  return str;
}

std::ostream & ROSException::printDescription(std::ostream &str) const {
  str << m_errorText;
  if (m_printSpecificDescription) {
    str << ", "
	<< m_specificDescription;
  }
  return str;
}

const std::string ROSException::getErrorMessage() const {
  std::ostringstream str;
  print(str);
  return (str.str()) ; 
}

const std::string ROSException::getDescription() const {
  std::ostringstream str;
  printDescription(str);
  return str.str();
}
