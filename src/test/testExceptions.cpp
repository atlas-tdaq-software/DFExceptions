#include "DFExceptions/BaseException.h"
#include "DFExceptions/ROSException.h"
#include "TestException.h"
#include <iostream> 
 
class TestClass {
public:
  static void testMethod1() {
    throw TestException(TestException::TEST_ERROR1,"This is only a test!!") ;
  }
  static void testMethod2() {
    throw TestException(TestException::TEST_ERROR2) ;
  }
};

int main() {

  std::cout << "Catching TestException...." << std::endl << std::endl ;
  try {
    TestClass::testMethod1() ;
    std::cout << "No exception thrown" << std::endl ;
  }
  catch (TestException &e) {    
  std::cout << e << std::endl ;
  std::cout << "Package: "     << e.getPackage()     << std::endl 
	    << "Error Id: "    << e.getErrorId()     << std::endl 
	    << "Description: " << e.getDescription() << std::endl ;
  }
  std::cout << std::endl ;

  std::cout << "Catching ROSException...." << std::endl << std::endl ;
  try {
    TestClass::testMethod1() ;
    std::cout << "No exception thrown" << std::endl ;
  }
  catch (ROSException &e) {    
  std::cout << e << std::endl ;
  std::cout << "Package: "     << e.getPackage()     << std::endl 
	    << "Error Id: "    << e.getErrorId()     << std::endl 
	    << "Description: " << e.getDescription() << std::endl ;
  }
  std::cout << std::endl ;

#if 0
  //Now test catching the base exception
  std::cout << "Catching BaseException...." << std::endl << std::endl ;
  try {
    TestClass::testMethod1() ;
    std::cout << "No exception thrown" << std::endl ;
  }
  catch (BaseException &e) {    
	  std::cout << e << std::endl ;
	  std::cout << "Description: " << e.getErrorMessage() << std::endl ;
	  std::cout << e.what() << std::endl;
  }
#endif

  //Now test catching the std exception
  std::cout << "Catching std::exception...." << std::endl << std::endl ;
  try {
    TestClass::testMethod1() ;
    std::cout << "No exception thrown" << std::endl ;
  }
  catch (std::exception &e) {    
	  std::cout << e.what() << std::endl;
  }

  std::cout << std::endl ;
  std::cout << "Catching TestException with no user description...." << std::endl << std::endl ;
  try {
    TestClass::testMethod2() ;
    std::cout << "No exception thrown" << std::endl ;
  }
  catch (TestException &e) {    
	  std::cout << e << std::endl ;
    std::cout << "Package: " << e.getPackage() << std::endl 
	 << "Error Id: " << e.getErrorId() << std::endl 
	 << "Description: " << e.getDescription() << std::endl ;
  }



  return 0;
}
