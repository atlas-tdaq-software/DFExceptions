// -*- c++ -*-
/*
  ATLAS TEST Software

  Class: TESTEXCEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef TESTEXCEPTION_H
#define TESTEXCEPTION_H

#include "../../DFExceptions/ROSException.h"
#include <string>
#include <iostream>

class TestException : public ROSException {
  
public:
  enum ErrorCode { TEST_ERROR1, TEST_ERROR2} ;   

  TestException(ErrorCode error) ;
  TestException(ErrorCode error,std::string description) ;
  TestException(ErrorCode error,std::string file,int line) ;
  TestException(ErrorCode error,std::string description,std::string file,int line) ;

protected:
  virtual std::string getErrorString(unsigned int errorId) const;

};

inline TestException::TestException(TestException::ErrorCode error) 
   : ROSException("TestPackage",error,getErrorString(error)) { }

inline TestException::TestException(TestException::ErrorCode error,std::string description) 
  : ROSException("TestPackage",error,getErrorString(error),description) { }


inline std::string TestException::getErrorString(unsigned int errorId) const {
	std::string rc;    
  switch (errorId) {  
  case TEST_ERROR1:
    rc = "first test error" ;
    break;
  case TEST_ERROR2:
    rc = "second test error" ;
    break;
  default:
    rc = "" ;
    break;
  }
  return rc;
}

#endif //TESTEXCEPTION_H
 
