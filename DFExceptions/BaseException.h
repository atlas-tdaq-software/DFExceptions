// -*- c++ -*-
/*
  ATLAS ROS Software

  Class: BASEEXCEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef BASEEXCEPTION_H
#define BASEEXCEPTION_H

#include <exception>
#include <string>
#include <string.h>  // for strcpy (why do we still have to deal with C style strings?)
#include <iostream>

class BaseException : public std::exception {
public:
  virtual const char * what() const noexcept;
  virtual const std::string getErrorMessage() const = 0;
  enum Severity { FATAL, RECOVERABLE, WARNING };

  friend std::ostream &operator<< (std::ostream &, BaseException &) ;
  BaseException() {} 
  virtual ~BaseException() {} 

private:
  virtual std::ostream & print(std::ostream &stream) const = 0;
};

inline const char * BaseException::what() const noexcept { 
  //This is to avoid possibly returning an invalid pointer
  //BEWARE: it is the caller's responsability to delete the 
  //output message
  char *output = new char[getErrorMessage().size()+1];
  return strcpy(output,getErrorMessage().c_str()) ; 
}

inline  std::ostream & operator<< (std::ostream &stream,BaseException &exception) { 
  return exception.print(stream) ; 
}

#endif //BASEEXCEPTION_H
 
