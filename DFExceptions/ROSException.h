// -*- c++ -*-
/*
  ATLAS ROS Software

  Class: ROSEXCEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef ROSEXCEPTION_H
#define ROSEXCEPTION_H

#include <sstream>
#include <string>
#include <iostream>

#include "ers/ers.h"
#include "ers/Issue.h"


class ROSException : public ers::Issue {
   template <class> friend class ers::IssueRegistrator;

public:  
  virtual const std::string getDescription() const;
  const std::string getPackage() const;
  unsigned int getErrorId() const;
  const std::string getErrorMessage() const;
  virtual ~ROSException() {} 
   ROSException(std::string package,unsigned int errorId,std::string errorText, const ers::Context& context); 
   ROSException(std::string package,unsigned int errorId,std::string errorText, std::string description, const ers::Context& context); 
  ROSException(std::string package,unsigned int errorId,std::string errorText);
  ROSException(std::string package,unsigned int errorId,std::string errorText,std::string description );

   ROSException(const std::exception& cause, std::string package, unsigned int error, std::string errorText,
                std::string description, const ers::Context& context) ;

protected:
   ROSException( const ers::Context & context );
   virtual void raise() const /* throw( std::exception ) */ { throw *this; }
   virtual ers::Issue * clone() const { return new ROSException( *this ); }

   static const char * get_uid() { return "ROSException"; }
   virtual const char * get_class_name() const { return get_uid(); }

  /* The following method must be defined in the 
   * specific package exceptions to return the 
   * error strings corresponding to the error ids
   */ 
   //  virtual std::string getErrorString(unsigned int errorId) const = 0;

private:

  const std::string m_package; 
  const unsigned int m_errorId;
   const std::string m_errorText;
  const std::string m_specificDescription;
  virtual std::ostream & print(std::ostream &stream) const;    
  std::ostream & printErrorMessage(std::ostream &stream) const;    
  std::ostream & printDescription(std::ostream &stream) const;    
  bool m_printSpecificDescription ;
  bool m_printFilename ;
  const std::string m_file ;
  const int m_line ;
};

inline std::ostream & ROSException::print(std::ostream &stream) const {
  return printErrorMessage(stream);  
}

#endif //ROSEXCEPTION_H
 
